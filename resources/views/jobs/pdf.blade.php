<html>
    <head>
        <style>
            tr {
                padding-bottom: 2em;
            }
        </style>
    </head>
    <body>
        <div class="table-responsive table-hover">
            <table class="table table-striped">
                <thead class="thead-inverse">
                <tr>
                    <th>Nursery</th>
                    <th>Description</th>
                    <th>Location</th>
                    <th>Priority</th>
                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td>{{$job->user->name}}</td>
                        <td>{{$job->desc}}</td>
                        <td>{{$job->location}}</td>
                        <td>{{$job->priority}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>
