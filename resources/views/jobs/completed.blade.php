@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Completed Jobs
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($jobs->count())
                <table class="table table-condensed table-striped" id="myTable">
                    <thead>
                    <tr>
                        <th>NURSERY</th>
                        <th>DESC</th>
                        <th>LOCATION</th>
                        <th>Priority</th>
                        <th class="text-right">OPTIONS</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($jobs as $job)
                        <tr>
                            <td>{{$job->user->name}}</td>
                            <td>{{$job->desc}}</td>
                            <td>{{$job->location}}</td>
                            <td>{{$job->priority}}</td>
                            <td class="text-right">
                                <form action="{{ route('jobs.restore', $job->id) }}" method="POST" style="display: inline;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-ok"></i> Un-Complete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection