@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Jobs / Edit #{{$job->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('jobs.update', $job->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('desc')) has-error @endif">
                       <label for="desc-field">Desc</label>
                    <input type="text" id="desc-field" name="desc" class="form-control" value="{{ $job->desc }}"/>
                       @if($errors->has("desc"))
                        <span class="help-block">{{ $errors->first("desc") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('location')) has-error @endif">
                       <label for="location-field">Location</label>
                    <input type="text" id="location-field" name="location" class="form-control" value="{{ $job->location }}"/>
                       @if($errors->has("location"))
                        <span class="help-block">{{ $errors->first("location") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('priority')) has-error @endif">
                        <label for="priority-field">Priority</label>
                        <select name="priority" id="priority-field" class="form-control">
                            <option value="Low">Low</option>
                            <option value="Normal">Normal</option>
                            <option value="High">High</option>
                        </select>
                        @if($errors->has("location"))
                            <span class="help-block">{{ $errors->first("location") }}</span>
                        @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('jobs.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection