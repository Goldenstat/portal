@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Jobs
            <a class="btn btn-success pull-right" href="{{ route('jobs.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($jobs->count())
                <table class="table table-condensed table-striped table-responsive" id="myTable">
                    <thead>
                        <tr>
                            <th><a href="#" id="selectAll"><i class="fa fa-plus-circle"></i></a></th>
                            <th>NURSERY</th>
                            <th>DESC</th>
                            <th>LOCATION</th>
                            <th>Priority</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($jobs as $job)
                            <tr>
                                <td><input type="checkbox" name="print" value="{{$job->id}}"></td>
                                <td>{{$job->user->name}}</td>
                                <td>{{$job->desc}}</td>
                                <td>{{$job->location}}</td>
                                <td>{{$job->priority}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-warning" href="{{ route('jobs.edit', $job->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('jobs.destroy', $job->id) }}" method="POST" style="display: inline;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-ok"></i> Complete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#selectAll').click(function() {
                $('input[name=print]').attr('checked', true);
            });
        });
        $('#Print').click(function() {
            var data = {};
            var dataStrings = [];

            $('input[type="checkbox"]').each(function() {
                if (this.checked) {
                    if (data[this.name] === undefined) data[this.name] = [];
                    data[this.name].push(this.value);
                }
            });

            $.each(data, function(key, value)
            {
                dataStrings.push(value.join('_'));
            });

            var result = dataStrings.join('&');

            window.location.href = "jobs/print/" + result;
        });
    </script>
@endsection