@extends('layout')

@section('content')
    <div class="container">

        @include('error')

        <form method="POST" action="/password/email">
            {!! csrf_field() !!}
            <h2 class="form-signin-heading">Password Reset</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" value="{{old('email')}}" required autofocus>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
        </form>
    </div>
@stop