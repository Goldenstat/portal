@extends('layout')

@section('content')
    <div class="container">

        @include('error')

            <form method="POST" action="/password/reset">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                <h2 class="form-signin-heading">Please reset your password</h2>
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" value="{{ old('email') }}" autofocus required>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <label for="inputPassword_conf" class="sr-only">Password</label>
                <input type="password" name="password_confirmation" id="inputPassword_conf" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
            </form>
    </div>
@stop