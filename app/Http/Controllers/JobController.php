<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;

use App\Job;
use App\User;
use Illuminate\Http\Request;

class JobController extends Controller {
    /**
     * JobController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        if(auth()->user()->name == 'Gaz' || auth()->user()->name == 'Robert Dunne')
        {
            $jobs = Job::with('user')->orderBy('id', 'desc')->get();
        } else {
            $jobs = Job::with('user')->where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        }

		return view('jobs.index', compact('jobs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        if(auth()->user()->name == 'Gaz' || auth()->user()->name == 'Robert Dunne')
        {
            $users = User::where('name', '!=', 'Robert Dunne')->where('name', '!=', 'Gaz')->get(['id', 'name']);
        } else {
            $users = null;
        }

		return view('jobs.create', compact('users'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$job = new Job();

		$job->desc = $request->input("desc");
        $job->location = $request->input("location");
		$job->priority = $request->input("priority");

        if(auth()->user()->name == 'Gaz')
        {
            $job->user_id = $request->input("user");
        } else {
            $job->user_id = auth()->user()->id;
        }

		$job->save();

		return redirect()->route('jobs.index')->with('message', 'Job has been created.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$job = Job::findOrFail($id);

		return view('jobs.show', compact('job'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$job = Job::findOrFail($id);

		return view('jobs.edit', compact('job'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$job = Job::findOrFail($id);

		$job->desc = $request->input("desc");
        $job->location = $request->input("location");
        $job->priority = $request->input("priority");

		$job->save();

		return redirect()->route('jobs.index')->with('message', 'Job updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$job = Job::findOrFail($id);
		$job->delete();

		return redirect()->route('jobs.index')->with('message', 'Job is marked as Complete.');
	}


    /**
     * Show page for completed Jobs
     *
     * @return Response
     */
    public function completed()
    {
        if(auth()->user()->name == 'Gaz' || auth()->user()->name == 'Robert Dunne')
        {
            $jobs = Job::with('user')->onlyTrashed()->orderBy('id', 'desc')->get();
        } else {
            $jobs = Job::with('user')->onlyTrashed()->where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        }

        return view('jobs.completed', compact('jobs'));
    }

    public function restore($id)
    {
        $job = findOrFail($id);
        $job->restore();

        return redirect()->route('jobs.complete')->with('message', 'Job has been Un-Completed');
    }

	public function printImages($data)
	{
		$data = explode('_', $data);
		$data = ['data' => $data];
		foreach($data as $id)
		{
			$job = Job::findorFail($id);
			$jobs['jobs'] = $job;
		}
		$pdf = PDF::loadView('jobs.pdf', $jobs);
		return $pdf->stream('details.pdf');
	}
}
